# -*- coding: utf-8 -*-
import random
import simpy

RANDOM_SEED = 4217
PT_MEAN = 20.0         # среднее время изготовления на станке
PT_SIGMA = 4.0         # ср.квадр.откл. времени изготовления на станке
MTTF = 600.0           # среднее время между поломками (Mean Time To Failure)
REPAIR_TIME = 30.0     # среднее время ремонта станка
NUM_MACHINES = 5       # количество станков
WEEKS = 2              # время моделирования в неделях
SIM_TIME = WEEKS * 7 * 24 * 60  # время моделирования в минутах

num_repair =[]      # список кол-ва ремонтов
fix_duration =[]    # список времен простоя
fix_times =[]       # список времен ремонтов
at_work = False     # работник на работе?
busy = False     # работник на работе?

men_at_work = []
men_busy = []
timelist = []
s = []

class Machine(object):
    def __init__(self, env, num, repairman):
        self.env = env
        self.name = 'Prn_'+str(num+1)
        self.parts_made = 0
        self.broken = False  # станок не сломан
        self.numb = num

        # запуск процессов работы и поломки станка
        self.process = env.process(self.working(repairman))
        env.process(self.break_machine())

    def working(self, repairman):
        global num_repair, fix_duration, fix_times, busy
        while True:
            # начало изготовления новой детали
            done_time = random.normalvariate(PT_MEAN, PT_SIGMA)
            while done_time:
                try:
                   # делаем деталь
                   start = self.env.now
                   yield self.env.timeout(done_time)
                   done_time = 0  # все доделано! =0
                except simpy.Interrupt:
                    self.broken = True  # станок сломан!
                    break_time = self.env.now  # время поломки станка
                    done_time -= self.env.now - start  # сколько времени осталось делать деталь
                    # требуется ремонтник, будем его ждать
                    with repairman.request(priority=10) as req:
                        yield req
                        for i in range(random.randint(1,3)):
                            detail = yield store.get()
                        num_repair[self.numb] +=1
                        fix_time = self.env.now  # время начала ремонта
                        busy = True
                        yield self.env.timeout(REPAIR_TIME)
                    self.broken = False   # станок восстановлен!
                    busy = False
                    fix_times[self.numb] += int(self.env.now - fix_time)
                    fix_duration[self.numb] += int(self.env.now - break_time)
            # станок сделал деталь
            self.parts_made += 1

    def break_machine(self):
        # поломка станка
        while True:
           yield self.env.timeout(random.expovariate(1.0/MTTF))
           # можно ломать только работающий станок!!
           if not self.broken: self.process.interrupt()

def work_sched(env, repairman):
    global at_work
    rep =[0,]*5
    # ремонтник выйдет на работу в 8ч
    at_work = False
    day_start = 8*60
    # до 8ч - отдых!
    for r in range(repairman.capacity):
       rep[r] = repairman.request(priority=1)
       yield rep[r]
    while True:
        # начинает новый день
        yield env.timeout(day_start)
        for r in range(repairman.capacity): repairman.release(rep[r])
        at_work = True
        day_work = 8*60
        # рабочее время началось
        workperiod=env.now
        yield env.timeout(day_work)
        at_work = False
        # рабочее время закончилось
        for r in range(repairman.capacity):
           rep[r] = repairman.request(priority=1)
           yield rep[r]
        # может быть переработка!
        day_start = 16*60 - (env.now-workperiod-day_work)

# специальная функция для сбора данных
def monitor(ev):
   global men_at_work, timelist, at_work, busy, men_busy
   while True:
      #состояние склада
      s.append(len(store.items))
      # запомним состояние работника
      men_at_work.append(int(at_work))
      men_busy.append(int(busy))
      timelist.append(ev.now)
      yield ev.timeout(5.0)


# настройка и выполнение модели
random.seed(RANDOM_SEED)  # начальное значение ДСЧ
# создание среды и настройка процессов модели
env = simpy.Environment()
repairman = simpy.PriorityResource(env, capacity=2)
machines = [Machine(env, i, repairman) for i in range(NUM_MACHINES)]
env.process(work_sched(env, repairman))
env.process(monitor(env))
store = simpy.Store(env, capacity=888)
store.items = [1,]*500

fix_duration =[0,] *len(machines) # делаем массив времен простоя станков
fix_times =[0,] *len(machines) # делаем массив времен ремонтов станков
num_repair =[0,] *len(machines) # делаем массив числа ремонтов станков

# запуск модели
env.run(until=SIM_TIME)

# вывод результатов
print(f'Результаты работы Мастерской за {WEEKS} недели/{SIM_TIME} мин')
for mach in machines:
    print(f'станок {mach.name} сделал {str(mach.parts_made)} деталей')
print('Количество ремонтов:', num_repair)
print('Общее время ремонтов:', sum(fix_times))
print('Время простоя каждого станка:', fix_duration)
print('Склад=' + str(len(store.items)))

try:
    import matplotlib.pyplot as plt
    plt.rcdefaults()
    fig, ax = plt.subplots(2, 1) # 2 диаграммы на одну панель
    # График состояния работника
    ax[0].step(timelist,men_at_work,lw=2,c='r')
    ax[0].step(timelist,men_busy,lw=0.5,c='y')

    ax[0].set_title('режим работы')
    ax[0].set_xlabel('время')
    ax[0].set_ylabel('состояние')
    ax[0].set_yticks([0,1], ['дома', 'на работе'])
    ax[1].step(timelist,s,lw=2,c='b')
    
#    fig, axis = plt.subplots(1,4)
#    # диаграмма сделанных деталей
#    fig.suptitle('диаграмма сделанных деталей')
#    lab=[]; lab1=[]; val=[]; sumpart=0
#    for m in machines:
#       lab.append(m.name)
#       lab1.append(m.name+'\n'+str(m.parts_made))
#       val.append(m.parts_made)
#       sumpart+=m.parts_made
#    #fig, bx = plt.subplots() # если надо на разных панелях
#    axis[0].set_title('сделано за 2 недели')
#    axis[0].pie(val, labels=lab, wedgeprops=dict(width=0.4,edgecolor='w'))
#    axis[1].set_title('сделано на 5 станках')
#    axis[1].pie(val, labels=lab1, wedgeprops=dict(width=0.7,edgecolor='w'))
#    axis[2].set_title('всего сделано '+ str(sumpart))
#    axis[2].pie(val, labels=lab1, counterclock=False)
#    axis[3].pie(val, labels=lab, wedgeprops=dict(width=1,edgecolor='y'))

    fig, axis = plt.subplots(1, 5)
    # диаграмма сделанных деталей
    fig.suptitle('состояние станков за время моделирования - рабочее/поломка')
    lab = [];
    val1 = [];
    val2 = [];
    val3 = [];
    val4 = [];
    val5 = [];

    sumpart = 0
    for m in machines:
        lab.append(m.name)

    val1.append(fix_duration[0]-fix_times[0])
    val1.append(fix_times[0])
    val1.append(SIM_TIME-fix_duration[0])

    val2.append(fix_duration[1] - fix_times[1])
    val2.append(fix_times[1])
    val2.append(SIM_TIME-fix_duration[1])

    val3.append(fix_duration[2] - fix_times[2])
    val3.append(fix_times[2])
    val3.append(SIM_TIME-fix_duration[2])

    val4.append(fix_duration[3] - fix_times[3])
    val4.append(fix_times[3])
    val4.append(SIM_TIME-fix_duration[3])

    val5.append(fix_duration[4] - fix_times[4])
    val5.append(fix_times[4])
    val5.append(SIM_TIME-fix_duration[4])

    # fig, bx = plt.subplots() # если надо на разных панелях
    axis[0].set_title(lab[0])
    axis[0].pie(val1, labels=val1, wedgeprops=dict(width=0.7, edgecolor='w'))
    axis[1].set_title(lab[1])
    axis[1].pie(val2, labels=val2, wedgeprops=dict(width=0.7, edgecolor='w'))
    axis[2].set_title(lab[2])
    axis[2].pie(val3, labels=val3, wedgeprops=dict(width=0.7, edgecolor='w'))
    axis[3].set_title(lab[3])
    axis[3].pie(val4, labels=val4, wedgeprops=dict(width=0.7, edgecolor='w'))
    axis[4].set_title(lab[4])
    axis[4].pie(val5, labels=val5, wedgeprops=dict(width=0.7, edgecolor='w'))

    #axis[0].axis("equal")
    fig.set_figwidth(15)    #  ширина и
    fig.set_figheight(6)    #  высота "Figure"
    fig.set_facecolor('floralwhite')
    plt.show()
    #plt.savefig('grafik.png')
except ImportError:
    print('Результаты работы можно увидеть если поставить matplotlib')
