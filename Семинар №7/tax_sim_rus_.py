import simpy
import random

# Константы
TAXI_TRIPS = 10
DEPARTURE_INTERVAL = 5
SIM_TIME = 100

# Функция, которая моделирует поведение такси
def taxi_process(env, taxi_id, trip_time):
    trips = 0 # счетчик количества поездок
    total_trip_time = 0 # счетчик суммарной длительности поездок с пассажиром

    while True:
        # такси приезжает на остановку
        print(f"Такси {taxi_id} прибыло на остановку в {env.now}")
        yield env.timeout(1)

        # такси ждет пассажира
        print(f"Такси {taxi_id} ожидает пассажира в {env.now}")
        yield env.timeout(random.randint(1, 5))

        # такси перевозит пассажира
        print(f"Такси {taxi_id} начинает поездку в {env.now}")
        yield env.timeout(trip_time)

        # такси прибывает на место назначения
        print(f"Такси {taxi_id} прибыло на место назначения в {env.now}")
        trips += 1 # увеличиваем счетчик количества поездок
        total_trip_time += trip_time # увеличиваем счетчик суммарной длительности поездок с пассажиром

        # такси уезжает в гараж
        print(f"Такси {taxi_id} уехал в гараж в {env.now}")
        break

    # возвращаем значения счетчиков
    return trips, total_trip_time

# Основная часть программы
env = simpy.Environment()

# Создаем несколько такси и запускаем моделирование
num_taxis = 5
taxis = {}
for i in range(num_taxis):
    trip_time = (i+1)*3
    taxis[i] = env.process(taxi_process(env, i, trip_time))

env.run(until=SIM_TIME)

# выводим значения счетчиков на консоль
for taxi_id in taxis:
    trips, total_trip_time = taxis[taxi_id].value
    print(f"Такси {taxi_id} совершило {trips} поездок, общее время в пути с пассажиром: {total_trip_time}")
